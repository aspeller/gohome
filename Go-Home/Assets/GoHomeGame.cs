﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoHomeGame : MonoBehaviour
{

    public Vector2 playerLocation; // distance in meters
    public Vector2 homeLocation;
    public Vector2 pathToHome;
    private bool isHome;

    // Use this for initialization
    void Start()
    {
        playerLocation = new Vector2(2.0f, 3.0f);
        homeLocation = new Vector2(2.0f, 9.0f);

        print("Welcome to Go-Home!");
        print("A game where you need to find  your way back home");

        ConsoleDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isHome)
        {
            UpdateMovement(KeyCode.LeftArrow, new Vector2(-1.0f, 0f), "Left");
            UpdateMovement(KeyCode.RightArrow, new Vector2(1.0f, 0f), "Right");
            UpdateMovement(KeyCode.UpArrow, new Vector2(0f, 1.0f), "Up");
            UpdateMovement(KeyCode.DownArrow, new Vector2(0f, -1.0f), "Down");
        }
        
    }

    private void UpdateMovement(KeyCode kc, Vector2 movement, string direction)
    {
        if (Input.GetKeyDown(kc))
        {
            print(direction);
            playerLocation += movement;
            print(playerLocation);
            ConsoleDisplay();
        }
    }

    private void ConsoleDisplay()
    {
        pathToHome = homeLocation - playerLocation;
        print("Current Location: " + playerLocation);
        print("Path to Home: " + pathToHome);
        print("Distance to home: " + pathToHome.magnitude);
        print("===========================================");
        if (pathToHome.magnitude == 0)
        {
            print("You made it home!");
            isHome = true;
        }
    }
}
